extern "C" {
    #include <Python.h>
}
#include <iostream>
#include <sstream>

using namespace std;

#include "cpp.hpp"

static string inside_cpp(string s)
{
    ostringstream stringStream;
    string r;

    // create new string
    stringStream << "we get in c++: " << s;
    r = stringStream.str();

    return r;
}

static PyObject*
py_inside(PyObject* self, PyObject* args)
{
    const char* s;

    if (!PyArg_ParseTuple(args, "s", &s))
        return NULL;

    return Py_BuildValue("s", inside_cpp(s).c_str());
}

static PyMethodDef module_methods[] =
{
     {"inside", py_inside, METH_VARARGS, "Say hello from C++."},
     {NULL, NULL, 0, NULL}
};

static struct PyModuleDef cpp_call =
{
    PyModuleDef_HEAD_INIT,
    "cpp_call", /* name of module */
    "Test calling from C++",          /* module documentation, may be NULL */
    -1,          /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
    module_methods
};

PyMODINIT_FUNC PyInit_cpp_call(void)
{
    return PyModule_Create(&cpp_call);
}
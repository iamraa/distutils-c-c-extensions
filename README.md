Examples C/C++ extensions for Python with distutils:

* C-extension
* C++-extension
* C-extension with Numpy C api

Статьи с комментариями:

* [Подключение C-кода в Python с помощью Distutils](http://quantrum.me/473-podklyuchenie-c-koda-v-python-s-pomoshhyu-distutils/)
* [Использование Numpy C api в C-расширении для Python](http://quantrum.me/511-ispolzovanie-numpy-c-api-v-c-rasshirenii-dlya-python/)
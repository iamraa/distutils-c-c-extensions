#include <Python.h>

#include <stdlib.h>
#include <stdio.h>

#include "c.h"

/* C-function */
static char* inside_c(const char* s)
{
    int required_size = 100;
    char *r = (char*)malloc(sizeof (char) * required_size); /* Reserve memory for return variable */

    /* Change string */
    sprintf(r, "we get: %s", s);

    //printf("%s", "print from C");

    return r;
}

/*
 * Enter point from Python
 */
static PyObject*
py_inside(PyObject* self, PyObject* args)
{
    const char* s;

    /* Parse arguments from Python:
       "s" - type string
       &s  - to variable s
     */
    if (!PyArg_ParseTuple(args, "s", &s))
        return NULL;

    /* Return tuple with strings
     */
    return Py_BuildValue("s", inside_c(s));
}

/* Array with methods
 */
static PyMethodDef module_methods[] =
{
     /* name from python, name in C-file, ..., __doc__ string of method */
     {"inside", py_inside, METH_VARARGS, "Say hello from C."},
     {NULL, NULL, 0, NULL}
};

/* Array with info about module to create it in Python
 */
static struct PyModuleDef c_call =
{
    PyModuleDef_HEAD_INIT,
    "c_call",               /* name of module */
    "Test calling from C",  /* module documentation, may be NULL */
    -1,                     /* size of per-interpreter state of the module,
                               or -1 if the module keeps state in global variables. */
    module_methods
};

/* Init our module in Python
 */
PyMODINIT_FUNC PyInit_c_call(void)
{
    return PyModule_Create(&c_call);
}